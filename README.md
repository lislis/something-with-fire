# Something with Fire

Smol thing build with [love2d](https://love2d.org).

The theme was *firestarter*

Going through the [hexagonal grid guide by red blob games](https://www.redblobgames.com/grids/hexagons/implementation.html).

Fire animation by [KerteX_ from OpenGameArt](https://opengameart.org/content/fireflame)

Click and fire will appear.

![screencap.gif](screencap.gif)
