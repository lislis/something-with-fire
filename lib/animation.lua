-- based on https://www.love2d.org/wiki/Tutorial:Animation

local Animation = {}

function Animation.new(image, width, height, duration)
   local animation = {}
   animation.spriteSheet = image;
   animation.quads = {};

   for y = 0, image:getHeight() - height, height do
      for x = 0, image:getWidth() - width, width do
         table.insert(animation.quads, love.graphics.newQuad(x, y, width, height, image:getDimensions()))
      end
   end

   animation.duration = duration or 1
   animation.currentTime = 0

   return animation
end

function Animation.update(anim, dt)
   anim.currentTime = anim.currentTime + dt
   if  anim.currentTime >=  anim.duration then
      anim.currentTime =  anim.currentTime - anim.duration
   end
end

function Animation.draw(anim, x, y, r, s)
   local spriteNum = math.floor(anim.currentTime / anim.duration * #anim.quads) + 1
   love.graphics.draw(anim.spriteSheet, anim.quads[spriteNum], x, y, r, s)
end

return Animation
