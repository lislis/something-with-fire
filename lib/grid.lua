-- grid helpers
Hex = require("lib/hex")
Layout = require("lib/layout")

local Grid = {}

function Grid.createHex(map_radius)
   grid = {}
   for q = - map_radius, map_radius do
      local r1 = math.max(-map_radius, -q - map_radius)
      local r2 = math.min(map_radius, -q + map_radius)
      for r = r1, r2 do
         table.insert(grid, Hex.new(q, r, -q -r))
      end
   end
   return grid
end


function unpack_vertices(vert)
   local list = {}
   for i = 1, #vert do
      table.insert(list, vert[i].x)
      table.insert(list, vert[i].y)
   end
   return list
end

function Grid.drawHex(layout, he, arg)
   local vertices = unpack_vertices(Layout.polygon_corners(layout, he))
   love.graphics.polygon(arg.mode, vertices)
end

function Grid.drawGrid(layout, grid, fires, arg)
   for i = 1, #grid do
      local he = grid[i]
      love.graphics.setColor(0.8, 0.8, 0.8)
      if Hex.eq(he, arg.mouse) then
         Grid.drawHex(layout, he, {mode = "fill"})
      else
         Grid.drawHex(layout, he, {mode="line"})
      end
   end

   for j = 1, #fires do
      local fi = fires[j]
      love.graphics.setColor(0.5, 0.05, 0.05)
      Grid.drawHex(layout, fi, {mode="fill"})
   end
end

return Grid
