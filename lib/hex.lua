-- Data structure to handle  hexagonal grids
-- implemented after https://www.redblobgames.com/grids/hexagons/implementation.html

local Hex = {}

function Hex.new(q, r, s)
   assert(not (math.floor (0.5 + q + r + s) ~= 0), "q + r + s must be 0")
   return {q = q, r = r, s = s}
end


function Hex.eq(a, b)
   if (a.q == b.q and a.r == b.r and a.s == b.s) then
      return true
   else
      return false
   end
end

function Hex.ne(a, b)
   return (not Hex.eq(a, b))
end

function Hex.add(a, b)
   return Hex.new(a.q + b.q, a.r + b.r, a.s + b.s)
end

function Hex.subtract(a, b)
   return Hex.new(a.q - b.q, a.r - b.r, a.s - b.s)
end

function Hex.mult(a, k)
   return Hex.new(a.q * k, a.r * k, a.s * k)
end

function Hex.length(hex)
   return math.floor((math.abs(hex.q) + math.abs(hex.r) + math.abs(hex.s)) / 2)
end

function Hex.distance(a, b)
   return Hex.length(Hex.subtract(a, b))
end

Hex.directions = { Hex.new(1, 0, -1), Hex.new(1, -1, 0),
                   Hex.new(0, -1, 1), Hex.new(-1, 0, 1),
                   Hex.new(-1, 1, 0), Hex.new(0, 1, -1) }

function Hex.direction(direction)
   assert (0 <= direction and  direction < 6)
   return Hex.directions[direction]
end
-- not in 0..5 ? Use this
-- hex_directions[(6 + (direction % 6)) % 6]

function Hex.neighbor(hex, direction)
   return Hex.add(hex, hex_direction(direction))
end

function Hex.round(h)
   local qi = math.floor(math.floor (0.5 + h.q))
   local ri = math.floor(math.floor (0.5 + h.r))
   local si = math.floor(math.floor (0.5 + h.s))
   local q_diff = math.abs(qi - h.q)
   local r_diff = math.abs(ri - h.r)
   local s_diff = math.abs(si - h.s)
   if q_diff > r_diff and q_diff > s_diff then
      qi = -ri - si
   else
      if r_diff > s_diff then
         ri = -qi - si
      else
         si = -qi - ri
      end
   end
   return Hex.new(qi, ri, si)
end

-- a and b are _not_ Hexes in this case
function lerp(a, b, t)
   return a * (1 - t) + b * t
end

function Hex.lerp(a, b, t)
   return Hex.new(lerp(a.q, b.q, t),
                  lerp(a.r, b.r, t),
                  lerp(a.s, b.s, t))
end

function Hex.linedraw(a, b)
   local N = Hex.distance(a, b)
   local results = {}
   local step = 1.0 / math.max(N, 1)
   for i = 0, N do
      table.insert(results, Hex.round(Hex.lerp(a, b, step * i)))
   end

   --local a_nudge = Hex(a.q + 0.000001, a.r + 0.000001, a.s - 0.000002)
   -- local b_nudge = Hex(b.q + 0.000001, b.r + 0.000001, b.s - 0.000002)
   --for i = 0, N do
   --   table.insert(results, hex_round(hex_lerp(a_nudge, b_nudge, step * i)))
   --end
   return results
end

return Hex
