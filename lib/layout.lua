-- draw hexagons on a screen
-- implemented after https://www.redblobgames.com/grids/hexagons/implementation.html

Orientation = require("lib/orientation")
Point = require("lib/point")

local Layout = {}

function Layout.new(orientation, size, origin)
   return { orientation = orientation, size = size, origin = origin }
end

function Layout.hex2pixel(layout, h)
   local M = layout.orientation
   local x = (M.f0 * h.q + M.f1 * h.r) * layout.size.x
   local y = (M.f2 * h.q + M.f3 * h.r) * layout.size.y
   return Point.new(x + layout.origin.x, y + layout.origin.y)
end

function Layout.pixel2hex(layout, p)
   local M = layout.orientation
   local pt = Point.new((p.x - layout.origin.x) / layout.size.x,
      (p.y - layout.origin.y) / layout.size.y)
   local q = M.b0 * pt.x + M.b1 * pt.y
   local r = M.b2 * pt.x + M.b3 * pt.y
   return Hex.new(q, r, -q -r)
end

function Layout.hex_corner_offset(layout, corner)
   local size = layout.size
   local angle = 2.0 * math.pi * (layout.orientation.start_angle + corner) / 6.0
   return Point.new(size.x * math.cos(angle), size.y * math.sin(angle))
end

function Layout.polygon_corners(layout, h)
   local corners = {}
   local center = Layout.hex2pixel(layout, h)
   for i= 0, 5 do
      local offset = Layout.hex_corner_offset(layout, i)
      local pt = Point.new(center.x + offset.x, center.y + offset.y)
      table.insert(corners, pt)
   end
   return corners
end

return Layout
