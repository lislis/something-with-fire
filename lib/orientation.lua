-- Orientation helper for hex grids

local Orientation = {}

function Orientation.new(f0, f1, f2, f3, b0, b1, b2, b3, start_angle)
   return { f0 = f0, f1 = f1, f2 = f2, f3 = f3,
            b0 = b0, b1 = b1, b2 = b2, b3 = b3,
            start_angle = start_angle }
end

Orientation.layout_pointy = Orientation.new(math.sqrt(3.0),
                                        math.sqrt(3.0) / 2.0,
                                        0.0,
                                        3.0 / 2.0,
                                        math.sqrt(3.0) / 3.0,
                                           -1.0 / 3.0,
                                        0.0,
                                        2.0 / 3.0,
                                        0.5)

Orientation.layout_flat = Orientation.new(3.0 / 2.0,
                                          0.0,
                                          math.sqrt(3.0) / 2.0,
                                          math.sqrt(3.0),
                                          2.0 / 3.0, 0.0,
                                          -1.0 / 3.0,
                                          math.sqrt(3.0) / 3.0,
                                          0.0)

return Orientation
