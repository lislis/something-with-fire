-- minimal Point

local Point = {}

function Point.new(x, y)
   return { x = x, y = y }
end

return Point
