Hex = require("lib/hex")
Orientation = require("lib/orientation")
Point = require("lib/point")
Layout = require("lib/layout")
Grid = require("lib/grid")
Animation = require("lib/animation")

function love.load()
   width, height = love.graphics.getDimensions()

   layout_orientation = Orientation.layout_pointy
   origin = Point.new(width / 2, height /2)
   pt_size =  Point.new(30, 30)
   the_layout = Layout.new(layout_orientation, pt_size, origin)

   radius = 4
   grid = Grid.createHex(radius)

   marked = {}
   fires = {}

   args = {}
   args.mode = "line"

   fire_anim = Animation.new(love.graphics.newImage("assets/Fiyah.png"), 58, 72, 1)
end

function love.update(dt)
   Animation.update(fire_anim, dt)
   mX, mY = love.mouse.getPosition()
   mouseHex = Layout.pixel2hex(the_layout, Point.new(mX, mY))
   args.mouse = Hex.round(mouseHex)
end


function love.draw()
   love.graphics.setBackgroundColor(0.1, 0.1, 0.1, 1)
   love.graphics.setLineWidth(2)
   Grid.drawGrid(the_layout, grid, fires, args)

   love.graphics.setColor(1, 1, 1)
   for f=1, #fires do
      local pt = Layout.hex2pixel(the_layout, fires[f])
      Animation.draw(fire_anim, pt.x - 20, pt.y - 27, 0, 0.7)
   end
end

function love.mousereleased(x, y, button, istouch, presses)
   mHex = Layout.pixel2hex(the_layout, Point.new(x, y))
   table.insert(fires, Hex.round(mHex))
end
